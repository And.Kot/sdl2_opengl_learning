#include "ShaderProgram.h"

#include <iostream>
#include <vector>

ShaderProgram::ShaderProgram()
    : programID(0), vertexShader(0), fragmentShader(0) {}

ShaderProgram::~ShaderProgram() {}

void ShaderProgram::CompileShaderFromSource(const char *inputVertexShader,
                                            const char *inputFragmentShader) {
  programID = glCreateProgram();

  vertexShader = glCreateShader(GL_VERTEX_SHADER);
  if (vertexShader == 0) {
    std::cout << "Error creating vertex shader" << std::endl;
  }

  fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  if (fragmentShader == 0) {
    std::cout << "Error creating fragment shader" << std::endl;
  }

  CompileShader(inputVertexShader, vertexShader);
  CompileShader(inputFragmentShader, fragmentShader);
}

void ShaderProgram::LinkShaders() {
  glAttachShader(programID, vertexShader);
  glAttachShader(programID, fragmentShader);

  glLinkProgram(programID);

  GLuint isLinked = 0;
  glGetProgramiv(programID, GL_LINK_STATUS, (int *)&isLinked);
  if (isLinked == GL_FALSE) {
    GLint infoLogSize = 0;
    glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infoLogSize);
    std::vector<char> errorInfo(infoLogSize);
    glGetProgramInfoLog(programID, infoLogSize, &infoLogSize, &errorInfo[0]);

    glDeleteProgram(programID);
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    std::cout << "LinkShader: " << std::endl;
    for (auto &i : errorInfo) {
      std::cout << i;
    }
    std::cout << std::endl;
  }

  glDetachShader(programID, vertexShader);
  glDetachShader(programID, fragmentShader);
  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader);
}

void ShaderProgram::Use() {
  glUseProgram(programID);

  for (int i = 0; i < attributeCount; ++i) {

    glEnableVertexAttribArray(i);
  }
}
void ShaderProgram::UnUse() {
  glUseProgram(0);
  for (int i = 0; i < attributeCount; ++i) {

    glDisableVertexAttribArray(i);
  }
}

void ShaderProgram::AddAttributes(const std::string &inputAttributeName) {
  ++attributeCount;
  glBindAttribLocation(programID, attributeCount, inputAttributeName.c_str());
}

void ShaderProgram::CompileShader(const char *source, GLuint id) {

  glShaderSource(id, 1, &source, NULL);

  glCompileShader(id);

  GLint compileStatus = 0;

  glGetShaderiv(id, GL_COMPILE_STATUS, &compileStatus);

  if (compileStatus == GL_FALSE) {
    GLint infoLogSize = 0;
    glGetShaderiv(id, GL_INFO_LOG_LENGTH, &infoLogSize);
    std::vector<char> errorInfo(infoLogSize);
    glGetShaderInfoLog(id, infoLogSize, &infoLogSize, &errorInfo[0]);

    glDeleteShader(id);

    std::cout << "CompileShader: " << std::endl;
    for (auto &i : errorInfo) {
      std::cout << i;
    }
    std::cout << std::endl;
  }
}
