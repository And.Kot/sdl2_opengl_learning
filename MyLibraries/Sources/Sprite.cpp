#include "Sprite.h"
#include "Vertices.h"

#include <cstddef>

Sprite::Sprite() { VB = 0; }

Sprite::~Sprite() {
  if (VB != 0) {
    glDeleteBuffers(1, &VB);
  }
}

void Sprite::Draw() {
  glBindBuffer(GL_ARRAY_BUFFER, VB);

  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2D),
                        (void *)offsetof(Vertex2D, position));

  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex2D),
                        (void *)offsetof(Vertex2D, color));

  glDrawArrays(GL_TRIANGLES, 0, 6);
  glDisableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Sprite::Init(float inputX, float inputY, float inputWidth,
                  float inputHeight) {
  x = inputX;
  y = inputY;
  width = inputWidth;
  height = inputHeight;

  if (VB == 0) {
    glGenBuffers(1, &VB);
  }

  Vertex2D vertexData[6];

  vertexData[0].SetPosition(x, y);
  vertexData[1].SetPosition(x + width, y);
  vertexData[2].SetPosition(x, y + height);
  vertexData[3].SetPosition(x, y + height);
  vertexData[4].SetPosition(x + width, y);
  vertexData[5].SetPosition(x + width, y + height);

  vertexData[0].SetColor(0, 255, 0, 255);
  vertexData[1].SetColor(255, 0, 255, 255);
  vertexData[2].SetColor(0, 255, 0, 255);
  vertexData[3].SetColor(0, 0, 255, 255);
  vertexData[4].SetColor(255, 0, 255, 255);
  vertexData[5].SetColor(0, 255, 0, 255);

  glBindBuffer(GL_ARRAY_BUFFER, VB);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
}
