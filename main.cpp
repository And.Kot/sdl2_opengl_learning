#include "Game.h"
#include "InputManager.h"

#include <SDL.h>

#include <iostream>

using namespace std;

int main(int, char **) {
  Game game;

  game.Create(800, 600);
  game.Play();

  return 0;
}
