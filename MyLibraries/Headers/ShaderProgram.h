#pragma once

#include <glew.h>

#include <string>

class __declspec(dllexport) ShaderProgram {
public:
  ShaderProgram();
  ~ShaderProgram();

  void CompileShaderFromSource(const char *, const char *);
  void LinkShaders();
  void Use();
  void UnUse();
  void AddAttributes(const std::string &);

private:
  void CompileShader(const char *, GLuint);
  int attributeCount;
  GLuint programID;
  GLuint vertexShader;
  GLuint fragmentShader;
};
