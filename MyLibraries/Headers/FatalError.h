#pragma once

#include <string>

class __declspec(dllexport) FatalError {
public:
  FatalError(std::string message);
};
