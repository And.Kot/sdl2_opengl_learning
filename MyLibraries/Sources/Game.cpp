#include "Game.h"
#include "FatalError.h"

#include <glew.h>

#include <iostream>
#include <string>

Game::Game() {
  std::cout << "Game" << std::endl;
  gameLoop = true;
  width = 1024;
  height = 768;
}

Game::~Game() {
  inputManager.Dispose();

  if (window) {
    SDL_DestroyWindow(window);
  }

  SDL_Quit();
}

void Game::Create(int inputWidth, int inputHeight) {
  width = inputWidth;
  height = inputHeight;

  gameLoop = true;
  Init();

  sprite.Init(-0.5, -0.5, 1, 1);
}

void Game::Play() {
  while (gameLoop) {
    Update();
    Render();
  }
}

void Game::Update() {

  inputManager.Update(event);

  if (event.type == SDL_MOUSEMOTION) {
    std::cout << "MouseXY: " << inputManager.GetMouseCoordinate().x << "; "
              << inputManager.GetMouseCoordinate().y << std::endl;
  }
  if (inputManager.exit) {
    gameLoop = false;
  }
}

void Game::Render() {
  glClearDepth(1.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  shaderProgram.Use();

  sprite.Draw();

  shaderProgram.UnUse();

  SDL_GL_SwapWindow(window);
}

void Game::Init() {
  if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
    FatalError("Error Initialisation SDL: ");
  }

  Uint32 flags = SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL;

  window = SDL_CreateWindow("SDL2_learning", SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, width, height, flags);
  if (!window) {
    FatalError("Error creating window");
  }

  SDL_GLContext glContext = SDL_GL_CreateContext(window);
  if (!glContext) {
    FatalError("Error creating glContext");
  }

  GLenum error = glewInit();
  if (error != GLEW_OK) {
    FatalError("Error initialisation glew");
  }

  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

  InitShader();

  glClearColor(0.3, 0.3, 0.2, 1);
}

void Game::InitShader() {

  const char *vertexShader = "#version 300 es \n"
                             "in vec2 vertexPosition; \n"
                             "in vec4 vertexColor; \n"
                             "out vec4 fragmentColor;\n"
                             "void main()\n"
                             "{\n"
                             "gl_Position.xy = vertexPosition;\n"
                             "gl_Position.z = 0.0;\n"
                             "gl_Position.w = 1.0;\n"
                             "fragmentColor = vertexColor;\n"
                             "}";

  const char *fragmentShader = "#version 300 es \n"
                               "precision highp float;\n"
                               "in vec4 fragmentColor;\n"
                               "out vec4 outColor;\n"
                               "void main()\n"
                               "{\n"
                               "outColor = fragmentColor;\n"
                               "}";

  shaderProgram.CompileShaderFromSource(vertexShader, fragmentShader);
  // shaderProgram.AddAttributes("vertexPosition");
  // shaderProgram.AddAttributes("vertexColor");
  shaderProgram.LinkShaders();
}
