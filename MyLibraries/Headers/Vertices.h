#pragma once

#include <glew.h>

struct Vertex2D {

  struct Position {
    float x, y;
  } position;

  struct Color {
    GLubyte r, g, b, a;
  } color;

  void SetPosition(float inputX, float inputY) {
    position.x = inputX;
    position.y = inputY;
  }

  void SetColor(GLubyte inputR, GLubyte inputG, GLubyte inputB,
                GLubyte inputA) {
    color.r = inputR;
    color.g = inputG;
    color.b = inputB;
    color.a = inputA;
  }
};
