#pragma once

#include "InputManager.h"
#include "ShaderProgram.h"
#include "Sprite.h"

#include <SDL.h>

class __declspec(dllexport) Game {
public:
  Game();
  ~Game();

  void Create(int, int);
  void Play();

private:
  void Init();
  void Render();
  void Update();
  void InitShader();

  bool gameLoop;

  unsigned int width;
  unsigned int height;

  SDL_Window *window;
  SDL_Event event;

  Sprite sprite;

  InputManager inputManager;

  ShaderProgram shaderProgram;
};
