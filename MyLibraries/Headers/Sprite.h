#include <glew.h>

#pragma once

class __declspec(dllexport) Sprite {
public:
  Sprite();
  ~Sprite();

  void Draw();

  void Init(float, float, float, float);

private:
  float x, y, width, height;

  GLuint VB;
};
