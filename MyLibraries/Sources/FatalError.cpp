#include "FatalError.h"

#include <SDL.h>

#include <cstdlib>
#include <iostream>

FatalError::FatalError(std::string message) {
  std::cout << "Fatal Error" << std::endl;
  std::exit(EXIT_FAILURE);
}
